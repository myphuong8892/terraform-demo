# alb.tf

resource "aws_alb" "ecs_alb" {
  name            = "${local.prefix}-alb"
  subnets         = [
    aws_subnet.public_a.id,
    aws_subnet.public_b.id,
  ]
  security_groups = [aws_security_group.ecs_service.id]
  load_balancer_type  = "application"
  tags = local.common_tags
}

resource "aws_alb_target_group" "ecs_tg" {
  name        = "${local.prefix}-tg"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"

  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = var.health_check_path
    unhealthy_threshold = "2"
  }
}

# Redirect all traffic from the ALB to the target group
resource "aws_alb_listener" "front_end" {
  load_balancer_arn = aws_alb.ecs_alb.id
  port              = var.app_port
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.ecs_tg.id
    type             = "forward"
  }
}
