variable "prefix" {
  default = "terrform-demo"
}
variable "project" {
  default = "terrform-demo"
}
variable "contact" {
  default = "myphuong8892@gmail.com"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "431610741011.dkr.ecr.us-east-1.amazonaws.com/terrform-demo:latest"
}
variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 80
}

variable "health_check_path" {
  default = "/"
}