output "dns_name" {
  description = "The DNS name of the load balancer."
  value       = aws_alb.ecs_alb.dns_name
}
